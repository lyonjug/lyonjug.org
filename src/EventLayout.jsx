import {Outlet, useMatches} from "react-router-dom"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faCalendar} from "@fortawesome/free-regular-svg-icons"
import {faDisplay, faLocationDot} from "@fortawesome/free-solid-svg-icons"
import {faMeetup} from "@fortawesome/free-brands-svg-icons"

export default () => {
    const current_page = useMatches().find(i => i.handle)?.handle
    const formatted_date = new Date(current_page.date).toLocaleDateString("fr-FR", {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'});

    return <>
        <div className="text-4xl">{current_page.title}</div>
        <div className="text-gray-500 my-8">
            <div><FontAwesomeIcon icon={faCalendar} fixedWidth className="mr-2"/>{formatted_date} à 19h</div>
            <div><FontAwesomeIcon icon={faLocationDot} fixedWidth className="mr-2"/><a href={current_page.address_link} className="underline decoration-gray-300">{current_page.address}</a></div>
            {current_page.meetup_event_id && <div><FontAwesomeIcon icon={faMeetup} fixedWidth className="mr-2"/><a href={"https://www.meetup.com/lyonjug/events/" + current_page.meetup_event_id} className="underline decoration-gray-300">Inscription & discussion</a></div>}
            {current_page.slides && current_page.slides.map(slides => <div key={slides}><FontAwesomeIcon icon={faDisplay} fixedWidth className="mr-2"/><a href={slides} className="underline decoration-gray-300">Slides</a></div>)}
        </div>
        <div className="prose max-w-none">
            <Outlet/>
        </div>
    </>
}
