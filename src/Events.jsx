import {Link} from "react-router-dom"
import events from "virtual:events"

const oldEvents = [
    {date: "2010-05-18", title: "API REST"},
    {date: "2010-04-20", title: "Play! Framework & Anniversaire"},
    {date: "2010-03-16", title: "OSGi & BIRT"},
    {date: "2010-02-23", title: "JavaEE 6"},
    {date: "2010-01-19", title: "Méthodes agiles"},
    {date: "2009-12-15", title: "Wicket & Grails"},
    {date: "2009-11-23", title: "Google avec Didier Girard"},
    {date: "2009-10-20", title: "JavaEE 6 & JPA 2"},
    {date: "2009-09-15", title: "Sonar & Kalistick"},
    {date: "2009-06-16", title: "GlassFish & Groovy"},
    {date: "2009-05-19", title: "Spring & JCR/JackRabbit"},
    {date: "2009-04-21", title: "Lancement du LyonJUG"},
]

export default () => <>
    {events
        .sort((a, b) => -a.date.localeCompare(b.date))
        .map(event => (
            <Link to={event.key} key={event.key}>
                <div className="text-sm text-gray-400">{event.date}</div>
                <div className="text-lg mb-4">{event.title}</div>
            </Link>
        ))}
    {oldEvents
        .map(event => (
            <div key={event.date} className="text-gray-400">
                <div className="text-sm text-gray-300">{event.date}</div>
                <div className="text-lg mb-4">{event.title}</div>
            </div>
        ))}
</>
