import {describe, expect, test} from "vitest"
import events from "virtual:events"
import {Set} from "immutable"

describe("an event", () => {
    test("must have a title property that is a string", () => {
        events.forEach(event => {
            expect(event).toHaveProperty("title")
            expect(event.title).toEqual(expect.any(String))
        })
    })
    test("must have an address property that is a string", () => {
        events.forEach(event => {
            expect(event).toHaveProperty("address")
            expect(event.address).toEqual(expect.any(String))
        })
    })
    test("must have an address_link property that is a string", () => {
        events.forEach(event => {
            expect(event).toHaveProperty("address_link")
            expect(event.address_link).toEqual(expect.any(String))
        })
    })
    test("may have a meetup_event_id property, but if it does, it must be a string", () => {
        events.forEach(event => {
            event.meetup_event_id && expect(event.meetup_event_id).toEqual(expect.any(String))
        })
    })
    test("may have a slides property, but if it does, it must be an array of strings", () => {
        events.forEach(event => {
            event.slides
            && expect(event.slides).toEqual(expect.any(Array))
            && event.slides.forEach(slides => expect(slides).toEqual(expect.any(String)))
        })
    })
    test("must have an intrinsic date property that is a string and matches a certain pattern", () => {
        events.forEach(event => {
            expect(event).toHaveProperty("date")
            expect(event.date).toMatch(/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)
        })
    })
    test("must have an intrinsic key property that is a string and matches a certain pattern", () => {
        events.forEach(event => {
            expect(event).toHaveProperty("key")
            expect(event.key).toMatch(/^[0-9a-z\-]*$/)
        })
    })
    test("can only have those properties : title, address, address_link, meetup_event_id, slides, date, key and import", () => {
        events.forEach(event => {
            expect(["title", "address", "address_link", "meetup_event_id", "slides", "date", "key", "import"]).toEqual(expect.arrayContaining(Object.keys(event)))
        })
    })
})

describe("events", () => {
    test("must have unique dates", () => {
        expect(new Set(events.map(event => event.date)).size).toEqual(events.length)
    })
    test("must have unique keys", () => {
        expect(new Set(events.map(event => event.key)).size).toEqual(events.length)
    })
})
