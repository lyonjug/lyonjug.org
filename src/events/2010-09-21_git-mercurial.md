---
title: "Git & Mercurial"
address: "Epitech, 86 Boulevard Marius Vivier-Merle, Lyon"
address_link: "https://goo.gl/maps/A5gE6yB1ygGReCQw5"
slides: ["https://www.slideshare.net/slideshow/presentation-dvcs-git-mercurial-au-lyonjug/5259321"]
---

### 19h : Lightning Talk

Agnès Crepet viendra nous parler de la création de l’antenne lyonnaise du JDuchess (réseau social féminin autour du développement Java).

### 19h30 : Systèmes de gestion de version décentralisée avec Git et Mercurial

La présentation débutera par un résumé de ce qu'apportent les systèmes de gestion de version décentralisée par rapport aux classiques CVS/Subversion.
S'en suivra un retour d'expérience sur le choix et la mise en place d'une solution DVCS en entreprise (migration, impact sur le processus de développement, retours utilisateur) comprenant une étude comparative détaillée entre Git et Mercurial (troll inside ;-) afin de vous aider à choisir le bon outil.
La qualité du support sur les différents systèmes d’exploitation et l'intégration aux environnements de développement Eclipse et Netbeans seront notamment abordées.
Nous concluerons sur l'impact sans cesse grandissant des forges sociales comme GitHub sur l'écosystème des projets open-source.

_Loïc Frering ([@loicfrering](https://x.com/loicfrering)) est responsable d'applications et expert technique membre du groupe Innovation Labs chez Atos Worldline.
Passionné des architectures logicielles et de développement Web, que ce soit en Java, PHP ou autres technologies, il aime découvrir et appliquer les meilleures pratiques des différents mondes.
Son activité open-source est variée mais se concentre particulièrement en PHP où il baigne dans les communautés des principaux frameworks tels que Zend, Symfony et Doctrine.
Fort de son expérience, il développe et met à disposition sa propre bibliothèque open-source ([losolib](https://github.com/loicfrering/losolib)) permettant de construire de solides architectures en PHP._
