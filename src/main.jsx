import ReactDOM from "react-dom/client"
import React from "react"
import {createBrowserRouter, redirect, RouterProvider} from "react-router-dom"
import Layout from "./Layout"
import events from "virtual:events"
import "./index.css"

const Home = React.lazy(() => import("./Home"))
const Events = React.lazy(() => import("./Events"))
const EventLayout = React.lazy(() => import("./EventLayout"))

ReactDOM.createRoot(document.getElementById("root")).render(
    <React.StrictMode>
        <RouterProvider router={createBrowserRouter([{
            element: <Layout/>,
            children: [{
                index: true,
                element: <Home/>
            }, {
                path: "events",
                children: [{
                    index: true,
                    element: <Events/>
                }, {
                    element: <EventLayout/>,
                    children: events.map(event => {
                        const Event = React.lazy(event.import)
                        return {
                            path: event.key,
                            element: <Event/>,
                            handle: event
                        }
                    })
                }]
            }, {
                path: "*",
                loader: () => redirect("/")
            }]
        }])}/>
    </React.StrictMode>
)
